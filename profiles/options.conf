# vim: set tw=80 et sw=4 sts=4 ts=4 fdm=marker fdr={{{,}}}

# {{{ General fancy things

*/* SUBOPTIONS: PLATFORM
*/* SUBOPTIONS: LIBC

*/* SUBOPTIONS: AMD64_CPU_FEATURES
*/* SUBOPTIONS: ARM_CPU_FEATURES
*/* SUBOPTIONS: X86_CPU_FEATURES

*/* SUBOPTIONS: PROVIDERS
*/* SUBOPTIONS: LINGUAS
*/* SUBOPTIONS: ENCODINGS

*/* SUBOPTIONS: INPUT_DRIVERS
*/* SUBOPTIONS: VIDEO_DRIVERS
*/* SUBOPTIONS: FPRINT_DRIVERS
*/* SUBOPTIONS: NUT_DRIVERS
*/* SUBOPTIONS: PCSC_DRIVERS

*/* SUBOPTIONS: EXTENSIONS
*/* SUBOPTIONS: EPIPHANY_EXTENSIONS
*/* SUBOPTIONS: PHP_EXTENSIONS
*/* SUBOPTIONS: POSTGRESQL_EXTENSIONS

*/* SUBOPTIONS: ERLANG_ABIS
*/* SUBOPTIONS: FFMPEG_ABIS
*/* SUBOPTIONS: LUA_ABIS
*/* SUBOPTIONS: PHP_ABIS
*/* SUBOPTIONS: PYTHON_ABIS
*/* SUBOPTIONS: RUBY_ABIS
*/* SUBOPTIONS: VALA_ABIS

*/* SUBOPTIONS: MODULES
*/* SUBOPTIONS: APACHE_MODULES
*/* SUBOPTIONS: CONTRIB_MODULES
*/* SUBOPTIONS: COURIER_MODULES
*/* SUBOPTIONS: NGINX_MODULES

*/* SUBOPTIONS: PLUGINS
*/* SUBOPTIONS: GSTREAMER_PLUGINS
*/* SUBOPTIONS: KIPI_PLUGINS
*/* SUBOPTIONS: KOPETE_PLUGINS

*/* SUBOPTIONS: KDE_PARTS

*/* SUBOPTIONS: ANT_DATA_TYPES
*/* SUBOPTIONS: ANT_SCRIPTING
*/* SUBOPTIONS: ANT_TASKS

*/* SUBOPTIONS: IM_PROTOCOLS

*/* SUBOPTIONS: P2P_NETWORKS

*/* SUBOPTIONS: COCKPIT_COMPONENTS

*/* SUBOPTIONS: POSTGRESQL_SERVERS

# Things in HIDDEN_SUBOPTIONS need to go in SUBOPTIONS too
*/* HIDDEN_SUBOPTIONS: PLATFORM AMD64_CPU_FEATURES ARM_CPU_FEATURES \
    X86_CPU_FEATURES LIBC

# suboptions in SUBOPTIONS_NO_DESCRIBE need to be listed in SUBOPTIONS as well.
# These suboptions will not have their descriptions displayed by default.
*/* SUBOPTIONS_NO_DESCRIBE: LINGUAS

# }}}

# {{{ Global defaults

# Ciaran McCreesh <ciaran.mccreesh@googlemail.com>
# PLATFORM and *_CPU_FEATURES values get unmasked in subprofiles
*/* PLATFORM: (-amd64) (-arm) (-x86)
*/* X86_CPU_FEATURES: (-3dnow) (-3dnowext) (-avx) (-avx2) (-fma3) (-fma4) (-mmx) (-mmx2) (-mmxext) (-sse) (-sse2) (-sse3) (-ssse3) (-sse4.1) (-sse4.2) (-sse4a) (-xop)
*/* AMD64_CPU_FEATURES: (-3dnow) (-3dnowext) (-avx) (-avx2) (-fma3) (-fma4) (-mmxext) (-sse3) (-ssse3) (-sse4.1) (-sse4.2) (-sse4a) (-xop)
*/* ARM_CPU_FEATURES: (-iwmmxt) (-neon)
*/* LIBC: (-glibc) (-musl)

# Enable parts by default
*/* PARTS: binaries configuration data development documentation libraries

# Wulf C. Krueger <philantrop@exherbo.org>
# Default Apache modules to make it work with its default httpd.conf.
*/* APACHE_MODULES: alias authz_host dir log_config logio mime unixd

# Pierre Lejeune <superheron@gmail.com>
# Drivers for Fprint library
*/* FPRINT_DRIVERS: aes1610 aes2501 aes4000 fdu2000 upeke2 upeksonly upektc upekts uru4000 vcom5s

# Pierre Lejeune <superheron@gmail.com>
# P2P networks supported by MLDonkey
*/* P2P_NETWORKS: bittorrent directconnect donkey donkeysui fasttrack filetp \
    gnutella gnutella2

# Pierre Lejeune <superheron@gmail.com>
# Pcsc drivers for Belgian eID middleware
*/* PCSC_DRIVERS: acr38u ccid

# Small install size, great benefit
*/* bash-completion vim-syntax

# Most people want to have this
*/* ncurses truetype

# Security options
*/* crypt openssl pam ssl tcpd

# We use systemd as default init system
*/* journald systemd

# jemalloc provides better performance than malloc
*/* jemalloc

# }}}

# {{{ Cross Compilation Options

# Saleem Abdulrasool <compnerd@compnerd.org>
*/* SUBOPTIONS: TARGETS

# Benedikt Morbach <moben@exherbo.org>
# mask deprecated targets here, they can be unmasked in specific profiles for
# compatibility if those still use them. All other targets should be unmasked
# for cross-compilation at all times
*/* TARGETS: (-arm-exherbo-linux-gnueabi)

# }}}

# {{{ Per-package defaults

# Wulf C. Krueger <philantrop@exherbo.org>, May, 3rd 2014
# Enable the pbin option on paludis as per
# http://lists.exherbo.org/pipermail/exherbo-dev/2014-April/001313.html
sys-apps/paludis pbin

# Wulf C. Krueger <philantrop@exherbo.org>, April, 16th 2016
# emacs is in the stages set and, thus, should use a minimal default configuration
app-editors/emacs -* PROVIDERS: -*

dev-scm/git curl

dev-lang/ruby berkdb gdbm

# Bo Ørsted Andresen <zlin@exherbo.org>
# Decent defaults for kde parts
app-office/calligra KDE_PARTS: sheets words

# Heiko Becker <heirecka@exherbo.org>
# Install wallpapers for Plasma by default
kde/breeze wallpapers

# libcanberra is checked first upstream and lighter for just playing a
# notification sound than phonon.
kde-frameworks/knotifications canberra
kde-frameworks/knotifyconfig canberra
# Matches the spell? dep of kdelibs:4
kde-frameworks/sonnet aspell

# vlc seems to be slightly preferred over gstreamer upstream.
media-libs/phonon vlc

# wpa_supplicant[systemd] needs dbus enabled
net-wireless/wpa_supplicant dbus

net-fs/nfs-utils nfsv4
www-servers/lighttpd pcre

# Heiko Becker <heirecka@exherbo.org>
# Matches the behavior of the non scm exheres
server-pim/akonadi mysql

# Ali Polatel <alip@exherbo.org>
# seccomp user filter is available for x86 and amd64 since Linux-3.5
sys-apps/sydbox (-seccomp)

# Wulf C. Krueger <philantrop@exherbo.org>
# Usually, we hard-enable udev. This option is *solely* to break a dep-cycle between
# systemd->util-linux->systemd. Do NOT introduce new udev options.
sys-apps/util-linux udev

# Jakob Nixdorf <flocke@shadowice.org>
# Same as above, only needed to break the cycle systemd->pciutils->systemd.
sys-apps/pciutils udev

# Wulf C. Krueger <philantrop@exherbo.org>
# Provide sane defaults for the virtual providers
virtual/blas                    PROVIDERS: OpenBLAS
virtual/cron                    PROVIDERS: cronie
virtual/dhcp-client             PROVIDERS: dhcpcd
virtual/javadoc                 PROVIDERS: icedtea7
virtual/libaacs                 PROVIDERS: libaacs
virtual/mta                     PROVIDERS: sendmail
virtual/mysql                   PROVIDERS: mariadb
virtual/notification-daemon     PROVIDERS: notification-daemon
virtual/pkg-config              PROVIDERS: pkgconf
virtual/sydbox                  PROVIDERS: sydbox
virtual/syslog                  PROVIDERS: rsyslog
virtual/zathura-pdf             PROVIDERS: zathura-pdf-poppler

# Kylie McClain <somasis@exherbo.org>
# Choose most popular providers for common system utilities
virtual/awk                     PROVIDERS: gnu
virtual/coreutils               PROVIDERS: gnu
virtual/cpio                    PROVIDERS: gnu
virtual/grep                    PROVIDERS: gnu
virtual/gzip                    PROVIDERS: gnu
virtual/man                     PROVIDERS: man-db
virtual/jdk                     PROVIDERS: openjdk-bin
virtual/sed                     PROVIDERS: gnu
virtual/tar                     PROVIDERS: gnu
virtual/unzip                   PROVIDERS: unzip
virtual/wget                    PROVIDERS: gnu

# Quentin "Sardem FF7" Glidic <sardemff7@exherbo.org>
# Sane default for text-based web browsers
app-text/xmlto                  PROVIDERS: lynx

# Heiko Becker <heirecka@exherbo.org>
# Use openssl as the default for packages providing the choice between openssl/libressl
*/*                             PROVIDERS: openssl
# Use libjpeg-turbo as the default for packages supporting both libjpeg-turbo and ijg-jpeg
*/*                             PROVIDERS: jpeg-turbo

# Marc-Antoine Perennou <keruspe@exherbo.org>
# Use systemd as the default for packages providing the choice between systemd-udevd/eudev
*/*                             PROVIDERS: systemd

# Danilo Spinella <danyspin97@protonmail.com>
# Use systemd as the default for packages providing the choice between systemd-logind/elogind
*/*                             PROVIDERS: systemd-logind

# Marc-Antoine Perennou <keruspe@exherbo.org>
# Use gtk3 by default when there's a choice between gtk3 and gtk2
*/*                             PROVIDERS: gtk3

# Johannes Nixdorf <mixi@exherbo.org>
# Use elfutils as the default provider for packages allowing a choice between elfutils/libelf
*/*                             PROVIDERS: elfutils

# Johannes Nixdorf <mixi@exherbo.org>
# Use libxml2 as the default providers for XML handling
*/*                             PROVIDERS: libxml2

# Timo Gurr <tgurr@exherbo.org>
# Use dhcpcd as the default for packages providing the choice between dhcpcd/dhcp(dhclient)
*/*                             PROVIDERS: dhcpcd

# Heiko Becker <heirecka@exherbo.org>
# Use ImageMagick as the default for packages providing a choice between
# ImageMagick/GraphicsMagick
*/*                             PROVIDERS: imagemagick

# Marc-Antoine Perennou <keruspe@exherbo.org>
# Default to the reference implementation for dbus-daemon
*/*                             PROVIDERS: dbus-daemon

# Timo Gurr <tgurr@exherbo.org>
# Use krb5 as the default for packages providing the choice between heimdal/krb5
*/*                             PROVIDERS: krb5

# Marvin Schmidt <marv@exherbo.org>
# glib-networking recommends using the GnuTLS backend
dev-libs/glib-networking        PROVIDERS: -* gnutls

# Rasmus Thomsen <Rasmus.thomsen@protonmail.com>
# Set sane default for which h264 encoder to choose by default
# X264 is faster at encoding and produces better files at lower bitrates
media/ffmpeg                    PROVIDERS: x264

# Timo Gurr <tgurr@exherbo.org>
# libplacebo recommends and prefers the shaderc SPIR-V compiler
media-libs/libplacebo           PROVIDERS: shaderc

# Quentin "Sardem FF7" Glidic <sardemff7@exherbo.org>
# Some default to get to work, it is in stages already
net-apps/NetworkManager         PROVIDERS: gnutls

# Kim Højgaard-Hansen <kimrhh@exherbo.org>
# Need a default when building in CI. Iproute2 should be preferred over deprecated tools (net-tools) anyway.
net-misc/openvpn                PROVIDERS: iproute2

# Marc-Antoine Perennou <keruspe@exherbo.org>
# Need a default provider
net/wireguard-tools             PROVIDERS: nftables

# Danilo Spinella .<danyspin97@protonmail.com>
# Need a default provider
net-misc/connman                PROVIDERS: iptables

# Heiko Becker <heirecka@exherbo.org>
# aom offers a de-/encoder and lossless support
media-libs/libavif              PROVIDERS: aom

# Quentin "Sardem FF7" Glidic <sardemff7@exherbo.org>
# Presumed options
dev-libs/at-spi2-core X
dev-libs/libepoxy X
# This should match mesa and libva below.
dev-libs/libglvnd X
gnome-bindings/gtkmm X
kde-frameworks/* X
x11-dri/freeglut X
x11-dri/mesa X
x11-libs/cogl X
x11-libs/gtk+ X
x11-libs/libva X
x11-libs/qtbase gui
x11-libs/qttools gui

# Maxime Sorin <maxime.sorin@clever-cloud.com>
# Default erlang version
*/* ERLANG_ABIS: 26

# Calvin Walton <calvin.walton@kepstin.ca>
# Default FFmpeg version
*/* FFMPEG_ABIS: 6

# Heiko Becker <heirecka@exherbo.org>
# Default lua version
*/* LUA_ABIS: 5.4

# Paul Seidler <pl.seidler@gmail.com>
# Default python version
*/* PYTHON_ABIS: 3.11

# Quentin "Sardem FF7" Glidic <sardemff7@exherbo.org>
# Default ruby version
*/* RUBY_ABIS: 3.1

# Marc-Antoine Perennou <keruspe@exherbo.org>
# Default php version
*/* PHP_ABIS: 8.2

# Marc-Antoine Perennou <keruspe@exherbo.org>
# Default vala version
*/* VALA_ABIS: 0.56

# Marc-Antoine Perennou <keruspe@exherbo.org>
# Default postgresql version
*/* POSTGRESQL_SERVERS: 16

# Bo Ørsted Andresen <zlin@exherbo.org>
# gcc threads should always be enabled unless a new toolchain is being bootstrapped
sys-devel/gcc threads

# Ridai Govinda Pombo <ridai.govinda@gmail.com>
# Default option for libdv
media-libs/libdv sdl

# Kylie McClain <somasis@exherbo.org>
# Enable CACert patch on dev-libs/nss by default, since we put cacert in the ca-certificates
# by default anyway and it leads to less confusion
dev-libs/nss cacert

# Wulf C. Krueger <philantrop@exherbo.org>
# The bindist option is completely broken
mail-client/thunderbird (-bindist)

# Kylie McClain <somasis@exherbo.org>
# sys-apps/busybox is often used for fixing broken systems, which [static] can be useful for
sys-apps/busybox static

# Julian Ospald <hasufell@posteo.de>
# Set sensible xml parser default, so gerrit doesn't break
dev-games/cegui tinyxml

# Marc-Antoine Perennou <keruspe@exherbo.org>
# Enable gnome-keyring's ssh-agent by default to be backward compatible
gnome-desktop/gnome-keyring ssh-agent

# Ali Polatel <alip@exherbo.org>
# Prefer fast process_vm_{read,write}v system calls rather than ptrace()
# You never want to disable this unless you have no other choice.
# E.g: You may lack the system calls before Linux-3.2 or
# after if CONFIG_CROSS_MEMORY_ATTACH=n
dev-libs/pinktrace pvm

# Heiko Becker <heirecka@exherbo.org>
# Simply prefer gdbm because it's in ::arbor and probably more common than
# tokyocabinet
mail-client/mutt gdbm

# Rasmus Thomsen <cogitri@exherbo.org>
# Prefer cairo to qt5 since it's almost always pulled in for GTK+ etc. anyway
app-text/poppler cairo

# Arthur Nascimento <tureba@gmail.com>
# Berkeley DB has some licensing issues, but has been the default for a while
mail-mta/postfix berkdb

# Timo Gurr <tgurr@exherbo.org>
# nghttp2 is mainly used for just the library. Disable the systemd option
# to make the CI happy.
net-libs/nghttp2 -systemd

# Gluzskiy Alexandr <sss@sss.chaoslab.ru>
# turn on zeroconf/avahi in kdnssd,  as it is main purpose of kdnssd, but create posibility to avoid avahi at all if needed
kde-frameworks/kdnssd zeroconf

# Rasmus Thomsen <cogitri@exherbo.org>
# Disable tcpd by default for gdm, it's only required for remote-login, which most
# users don't use. Makes CI happy.
gnome-desktop/gdm -tcpd

# Alexander Kapshuna <kapsh@kap.sh>
# Provide at least one audio output for Audacious player.
media-sound/audacious-plugins alsa

# Alexander Kapshuna <kapsh@kap.sh>
# Provide at least one audio output for DeadBeef player.
media-sound/deadbeef alsa

# Heiko Becker <heirecka@exherbo.org>
# Upstream defaults to LuaJit over plain lua, the latter is also masked
mail-filter/rspamd luajit

# Heiko Becker <heirecka@exherbo.org>
# Enable python_abis: 2.7 for packages which blacklist=3 and have a hard dep
# python:2.7
dev-python/functools32 python_abis: 2.7
dev-python/futures python_abis: 2.7
web-apps/mailman python_abis: 2.7

# Timo Gurr <tgurr@exherbo.org>
# Default NGINX modules to make it work with its default nginx.conf.
www-servers/nginx http

# Marvin Schmidt <marv@exherbo.org>
# kodi can be built using either X, gbm or wayland as graphics backend, select
# X by default to make CI happy
media/kodi X

# Timo Gurr <tgurr@exherbo.org>
# vulkan-tools can be built using either X or wayland as graphics backend, select
# X by default to make CI happy
sys-apps/vulkan-tools X

# Marvin Schmidt <marv@exherbo.org>
# Modern desktops use wayland, enable it by default
x11-dri/mesa wayland

# Marvin Schmidt <marv@exherbo.org>
# gtk needs either X or wayland to be enabled, select wayland to make CI happy
# Calvin Walton <calvin.walton@kepstin.ca>
# GTK upstream says that a supported build requires support for WebM media
# playback. The media option is presumed, so enable it by default.
x11-libs/gtk:4.0 media wayland

# Alexander Kapshuna <kapsh@kap.sh>
# pyqt is well known and stable choice
dev-python/QtPy PROVIDERS: pyqt5

# Timo Gurr <tgurr@exherbo.org>
# mesa >= 21.0 removed swrast
# Default to llvm and video_drivers:gallium-swrast to make CI happy and provide a
# sane fallback like we had with the hard-enabled non gallium based swrast before
*/* VIDEO_DRIVERS: gallium-swrast
x11-dri/mesa llvm

# Marvin Schmidt <marv@exherbo.org>
# Enable best supported Lua ABI for packages that don't support our default ABI
games-engines/openmw LUA_ABIS: 5.1
games-simulation/fs2_open LUA_ABIS: 5.1
media/mpv LUA_ABIS: 5.2

# Timo Gurr <tgurr@exherbo.org>
# Sane default man-db database backend
sys-apps/man-db gdbm

# Timo Gurr <tgurr@exherbo.org>
# Use qt5 by default when there's a choice between qt6 and qt5
*/* PROVIDERS: qt5

# Heiko Becker <heirecka@exherbo.org>
# Enable at least one audio format, supposedly the most common one
media-sound/mpd mp3

# Marvin Schmidt <marv@exherbo.org>
# libsoup-3.0 and libsoup-2.4 cannot be used in the same process and we use
# providers to select between the two during the transition phase where packages
# allow selecting either one.
# With GNOME 43 most packages transitioned to libsoup-3.0, so make it the default
*/* PROVIDERS: soup3

# Timo Gurr <tgurr@exherbo.org>
# Sane defaults, HTTP/2 is pretty common and standard these days, additionally it
# is required by rust
net-misc/curl http2

# Timo Gurr <tgurr@exherbo.org>
# Overwrite the default set above to use qt6 by default for obs to follow
# upstream recommendation
media-video/obs-studio PROVIDERS: -qt5 qt6

# Timo Gurr <tgurr@exherbo.org>
# Sane defaults, required by sys-apps/containerd (app-virtualization/moby)
sys-apps/runc seccomp

# Heiko Becker <heirecka@exherbo.org>
# Prefer defaults for Plasma5 until 6 becomes a real thing
kde-frameworks/libkscreen:5 runtime
kde-frameworks/baloo:5 indexer-service
kde-frameworks/kglobalaccel:5 runtime
kde-frameworks/kwallet:5 runtime
kde-frameworks/plasma-framework desktopthemes

# Calvin Walton <calvin.walton@kepstin.ca>
# Enable best supported FFmpeg ABI for packages that don't support our default
media/kodi[=20*] FFMPEG_ABIS: 4
media/mlt[=6*] FFMPEG_ABIS: 4
media-libs/ffms2 FFMPEG_ABIS: 4
media-libs/gegl FFMPEG_ABIS: 5
media-libs/mediastreamer FFMPEG_ABIS: 4
media-tv/tvheadend[<scm] FFMPEG_ABIS: 4
media-tv/tvheadend[~scm] FFMPEG_ABIS: 5
media-video/handbrake[=1.5*] FFMPEG_ABIS: 4

# Calvin Walton <calvin.walton@kepstin.ca>
# FFmpeg is reasonable default for gstreamer vp8/vp9 decoder, faster than libvpx
virtual/gst-plugin-vpx PROVIDERS: ffmpeg

# Timo Gurr <tgurr@exherbo.org>
# libblockdev is mainly used by udisks, set sane option defaults required by it
base/libblockdev cryptsetup mdraid nvme

# Calvin Walton <calvin.walton@kepstin.ca>
# webkit needs at least one of X or wayland to be enabled.
# Enable X on slots using gtk+:3 and wayland on slots using gtk:4.0 to make CI
# happy. The options selected match the defaults of the gtk versions.
net-libs/webkit:4.0 X
net-libs/webkit:4.1 X
net-libs/webkit:5.0 wayland
net-libs/webkit:6.0 wayland

# Timo Gurr <tgurr@exherbo.org>
# Default to using duktape instead of mozjs
sys-auth/polkit duktape

# Timo Gurr <tgurr@exherbo.org>
# Build PostgreSQL 16 with ICU support by default as upstream recommends.
dev-db/postgresql:16 icu

# Timo Gurr <tgurr@exherbo.org>
# MangoHud can be built with support for X and wayland, select X by default to
# make CI happy.
x11-monitoring/MangoHud X

# Timo Gurr <tgurr@exherbo.org>
# glfw can be built with support for X and wayland, select X by default to
# make CI happy.
media-libs/glfw X

# Tom Briden <tom@decompile.me.uk>
# Enable gcr's ssh-agent by default to be backwards compatible
gnome-desktop/gcr ssh-agent

# }}}

