# Copyright 2009 Mike Kelly
# Distributed under the terms of the GNU General Public License v2

MY_PV=$(ever replace_all _)

require github [ user=keplerproject tag=v${MY_PV} ] flag-o-matic \
    lua [ multiunpack=true whitelist="5.1 5.2 5.3 5.4" work=${PN}-${MY_PV} ]

SUMMARY="A lua filesystem library"
DESCRIPTION="
LuaFileSystem is a Lua library developed to complement the set of functions related to file systems
offered by the standard Lua distribution. It offers a portable way to access the underlying
directory structure and file attributes.
"
HOMEPAGE+=" https://keplerproject.github.io/${PN}"

LICENCES="MIT"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS=""

DEPENDENCIES="
    build:
        virtual/pkg-config
"

DEFAULT_SRC_COMPILE_PARAMS=( CC="${CC}" )

prepare_one_multibuild() {
    edo sed -e "s:\(CFLAGS=\):\1 ${CFLAGS} $(${PKG_CONFIG} --cflags lua-$(lua_get_abi)):" \
            -e "s:^\(LUA_VERSION =\).*:\1 $(lua_get_abi):" \
            -e 's:/usr/include:$(PREFIX)/include:' \
            -e "s|/usr/local|/usr/$(exhost --target)|" \
            -i "${WORKBASE}"/${MULTIBUILD_CLASS}/${MULTIBUILD_TARGET}/${PN}-${MY_PV}/config

    edo sed -e "s: lua : lua$(lua_get_abi) :" \
            -i "${WORKBASE}"/${MULTIBUILD_CLASS}/${MULTIBUILD_TARGET}/${PN}-${MY_PV}/Makefile
}

