# Copyright 2009 Hong Hao <oahong@gmail.com>
# Distributed under the terms of the GNU General Public License v2
# Based in part upon 'man-db-2.5.6.ebuild' from Gentoo, which is:
#     Copyright 1999-2009 Gentoo Foundation

require alternatives systemd-service
require autotools [ supported_autoconf=[ 2.7 ] supported_automake=[ 1.16 ] ]

SUMMARY="The online manual database"
DESCRIPTION="
man-db is an implementation of the standard Unix documentation system accessed using the man
command. It uses a Berkeley DB database in place of the traditional flat-text whatis databases.
man-db is used by several popular GNU/Linux distributions, including Debian, Ubuntu, and SuSE.
It also compiles and runs on a number of proprietary Unix systems.
"
HOMEPAGE="https://man-db.nongnu.org"
DOWNLOADS="mirror://savannah/${PN}/${PNV}.tar.xz"

LICENCES="GPL-3"
SLOT="0"
PLATFORMS="~amd64 ~armv7 ~armv8 ~x86"
MYOPTIONS="
    seccomp [[ description = [ Support for confining subprocesses using libseccomp ] ]]
    ( berkdb gdbm ) [[ number-selected = exactly-one ]]
    ( linguas: ast ca cs da de eo es fi fr id it ja ka ko nl pl pt pt_BR ro ru sr sv tr vi
               zh_CN zh_TW )
"

DEPENDENCIES="
    build:
        app-text/po4a [[ note = [ localized man-pages ] ]]
        sys-devel/gettext
        virtual/pkg-config
    build+run:
        user/man
        group/man
        app-arch/zstd
        dev-libs/libpipeline[>=1.5.0]
        sys-apps/groff
        sys-libs/zlib
        berkdb? ( sys-libs/db:=[<6] )
        gdbm? ( sys-libs/gdbm )
        seccomp? ( sys-libs/libseccomp )
"

AT_M4DIR=( m4 gl/m4 )

src_prepare() {
    # Disable new failing test, last checked: 2.12.0
    edo sed \
        -e '/manpath-slash/d' \
        -i src/tests/Makefile.am

    autotools_src_prepare
}

src_configure() {
    local myconf=(
        --enable-cache-owner=man
        --enable-nls
        --disable-setuid
        --with-sections='1 1p 8 2 3 3p 3pm 3type 4 5 6 7 9 0p tcl n l p o 1x 2x 3x 4x 5x 6x 7x 8x'
        --with-systemdsystemunitdir=${SYSTEMDSYSTEMUNITDIR}
        --with-systemdtmpfilesdir=${SYSTEMDTMPFILESDIR}
        --with-zstd=zstd
        $(option_with seccomp libseccomp)
    )

    if option berkdb ; then
        myconf+=( --with-db=db )
    else
        myconf+=( --with-db=gdbm )
    fi

    econf "${myconf[@]}"
}

src_install() {
    default

    dodoc docs/{HACKING.md,TODO}

    local alternatives=(
        man ${PN} 10
        /usr/$(exhost --target)/bin/man ${PN}.man
        /usr/$(exhost --target)/bin/apropos ${PN}.apropos
        /usr/$(exhost --target)/bin/whatis ${PN}.whatis
        /usr/share/man/man1/man.1 ${PN}.man.1
        /usr/share/man/man1/apropos.1 ${PN}.apropos.1
        /usr/share/man/man1/whatis.1 ${PN}.whatis.1
    )
    alternatives_for "${alternatives[@]}"
}

