# Copyright 2010-2017 Wulf C. Krueger <philantrop@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github
require systemd-service

export_exlib_phases src_install

SUMMARY="Performance/system monitoring tools for Linux"
DESCRIPTION="
iostat(1) reports CPU statistics and input/output statistics for devices, partitions
and network filesystems.
mpstat(1) reports individual or combined processor related statistics.
pidstat(1) reports statistics for Linux tasks (processes) : I/O, CPU, memory, etc.
sar(1) collects, reports and saves system activity information (CPU, memory, disks,
interrupts, network interfaces, TTY, kernel tables,etc.)
sadc(8) is the system activity data collector, used as a backend for sar.
sa1(8) collects and stores binary data in the system activity daily data file. It
is a front end to sadc designed to be run from cron.
sa2(8) writes a summarized daily activity report. It is a front end to sar designed
to be run from cron.
sadf(1) displays data collected by sar in multiple formats (CSV, XML, etc.) This
is useful to load performance data into a database, or import them in a spreadsheet
to make graphs.
tapestat(1) reports statistics for tapes connected to the system.
"
HOMEPAGE="https://${PN}.github.io/"
DOWNLOADS="${HOMEPAGE}${PN}-packages/${PNV}.tar.xz"

LICENCES="GPL-2"
SLOT="0"
MYOPTIONS="
    pcp [[ description = [ Support exporting to the PCP archive file format ] ]]
    sensors [[ description = [ Enable sensors support ] ]]
"

# TODO: add once tests are actually running:
#    build+test:
#        dev-libs/libxml2:2.0 [[ note = [ xmllint ] ]]
#        dev-libs/yajl [[ note = [ json_verify ] ]]
DEPENDENCIES="
    build:
        sys-devel/gettext
    build+run:
        pcp? ( monitor/pcp )
        sensors? ( sys-apps/lm_sensors )
    suggestion:
        virtual/cron
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    conf_dir=/etc/sysstat
    sa_lib_dir=/usr/$(exhost --target)/lib/sa
    --enable-collect-all
    --enable-compress-manpg
    --enable-copy-only
    --enable-install-cron
    --enable-nls
    --disable-stripping
    --disable-use-crond
    --with-systemdsleepdir=/usr/$(exhost --target)/lib/systemd/system-sleep
    --with-systemdsystemunitdir="${SYSTEMDSYSTEMUNITDIR}"
)

DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=(
    pcp
    sensors
)

_SYSSTAT_COMMON_MAKE_ARGS=(
    'NLS_DIR=$(datarootdir)/locale'
)

DEFAULT_SRC_COMPILE_PARAMS=( "${_SYSSTAT_COMMON_MAKE_ARGS[@]}" )
DEFAULT_SRC_INSTALL_PARAMS=( "${_SYSSTAT_COMMON_MAKE_ARGS[@]}" )

sysstat_src_install() {
    default

    # No need to install the licence.
    edo rm "${IMAGE}"/usr/share/doc/${PNVR}/COPYING
    dodoc -r contrib xml
    dodoc cron/{crontab,sysstat.crond,sysstat.cron.daily,sysstat.cron.hourly,sysstat.crond.sample,sysstat.crond.sample,crontab.sample}

    keepdir /var/log/sa
}

