# Copyright 2015 Saleem Abdulrasool <compnerd@compnerd.org>
# Distributed under the terms of the GNU General Public License v2

require gcc.gnu.org
require toolchain-runtime-libraries

export_exlib_phases src_unpack src_install

SUMMARY="GCC Quad-Precision Math Library"

LICENCES="GPL-2"
SLOT="$(ever major)"

MYOPTIONS="
    linguas:
        be ca da de el eo es fi fr hr id ja nl ru sr sv tr uk vi zh_CN zh_TW
"

DEPENDENCIES="
    build:
        sys-apps/texinfo
        sys-devel/gcc:${SLOT}
"

if [[ ${PV} == *_pre* ]] ; then
    ECONF_SOURCE="${WORKBASE}/gcc-$(ever major)-${PV##*_pre}/libquadmath"
elif [[ ${PV} == *-rc* ]] ; then
    ECONF_SOURCE="${WORKBASE}/gcc-${PV/rc/RC-}/libquadmath"
else
    ECONF_SOURCE="${WORKBASE}/gcc-${PV/_p?(re)/-}/libquadmath"
fi
WORK="${WORKBASE}/build/$(exhost --target)/libquadmath"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    CC=$(exhost --tool-prefix)gcc-${SLOT}
    CPP=$(exhost --tool-prefix)gcc-cpp-${SLOT}
    --disable-multilib
)

if ever at_least 13.0.1_pre20230122 ; then
    DEFAULT_SRC_CONFIGURE_PARAMS+=(
        --with-gcc-major-version-only
    )
fi

# NOTE(compnerd) libquadmath does not have a testsuite
RESTRICT="test"

libquadmath_src_unpack() {
    default
    edo mkdir -p "${WORK}"

    # TODO(compnerd) find a more elegant solution to this (potentially addressing one of the
    # upstream FIXMEs in the process)
    edo sed -e "s,toolexecdir=no,toolexecdir='\${libdir}',"         \
            -e "s,toolexeclibdir=no,toolexeclibdir='\${libdir}',"   \
            -i "${ECONF_SOURCE}/configure"

    edo sed -e "/libsubincludedir =/clibsubincludedir = /usr/$(exhost --build)/lib/gcc/\$(target_alias)/\$(gcc_version)/include"    \
            -i "${ECONF_SOURCE}/Makefile.in"
}

libquadmath_src_install() {
    default

    symlink_dynamic_libs ${PN}
    slot_dynamic_libs ${PN}
    slot_other_libs ${PN}.a ${PN}.la

    alternatives_for _${PN} ${SLOT} ${SLOT} \
        /usr/share/info/${PN}.info ${PN}-${SLOT}.info
}

