Upstream: yes, taken from release/2.37/master

From 9cc2f41e52079869b64ee94d81f10626838f63de Mon Sep 17 00:00:00 2001
From: Florian Weimer <fweimer@redhat.com>
Date: Mon, 3 Apr 2023 17:23:11 +0200
Subject: [PATCH 11/17] x86_64: Fix asm constraints in feraiseexcept (bug
 30305)

The divss instruction clobbers its first argument, and the constraints
need to reflect that.  Fortunately, with GCC 12, generated code does
not actually change, so there is no externally visible bug.

Suggested-by: Jakub Jelinek <jakub@redhat.com>
Reviewed-by: Noah Goldstein <goldstein.w.n@gmail.com>
(cherry picked from commit 5d1ccdda7b0c625751661d50977f3dfbc73f8eae)
---
 NEWS                             | 1 +
 sysdeps/x86_64/fpu/fraiseexcpt.c | 4 ++--
 2 files changed, 3 insertions(+), 2 deletions(-)

diff --git a/NEWS b/NEWS
index aaf2cfcfc3..9dd1534770 100644
--- a/NEWS
+++ b/NEWS
@@ -22,6 +22,7 @@ The following bugs are resolved with this release:
   [30125] dynamic-link: [regression, bisected] glibc-2.37 creates new
     symlink for libraries without soname
   [30151] gshadow: Matching sgetsgent, sgetsgent_r ERANGE handling
+  [30305] x86_64: Fix asm constraints in feraiseexcept
 
 Version 2.37
 
diff --git a/sysdeps/x86_64/fpu/fraiseexcpt.c b/sysdeps/x86_64/fpu/fraiseexcpt.c
index 924eed96a9..e7430a4158 100644
--- a/sysdeps/x86_64/fpu/fraiseexcpt.c
+++ b/sysdeps/x86_64/fpu/fraiseexcpt.c
@@ -33,7 +33,7 @@ __feraiseexcept (int excepts)
       /* One example of an invalid operation is 0.0 / 0.0.  */
       float f = 0.0;
 
-      __asm__ __volatile__ ("divss %0, %0 " : : "x" (f));
+      __asm__ __volatile__ ("divss %0, %0 " : "+x" (f));
       (void) &f;
     }
 
@@ -43,7 +43,7 @@ __feraiseexcept (int excepts)
       float f = 1.0;
       float g = 0.0;
 
-      __asm__ __volatile__ ("divss %1, %0" : : "x" (f), "x" (g));
+      __asm__ __volatile__ ("divss %1, %0" : "+x" (f) : "x" (g));
       (void) &f;
     }
 
-- 
2.40.0

