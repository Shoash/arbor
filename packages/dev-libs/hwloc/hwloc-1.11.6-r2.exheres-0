# Copyright 2013 Benedikt Morbach <moben@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

SUMMARY="Portable Hardware Locality is a portable abstraction of hierarchical architectures"
DESCRIPTION="
The Portable Hardware Locality (hwloc) software package provides a portable abstraction (across OS, versions, architectures, ...) of the hierarchical topology of modern architectures, including NUMA memory nodes, sockets, shared caches, cores and simultaneous multithreading. It also gathers various system attributes such as cache and memory information as well as the locality of I/O devices such as network interfaces, InfiniBand HCAs or GPUs. It primarily aims at helping applications with gathering information about modern computing hardware so as to exploit it accordingly and efficiently.
"
HOMEPAGE="http://www.open-mpi.org/projects/${PN}"
DOWNLOADS="http://www.open-mpi.org/software/hwloc/v$(ever range -2)/downloads/${PNV}.tar.bz2"

LICENCES="BSD-3"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS="
    graphics [[ description = [ Add graphical output to the lstopo command ] ]]
"

# rebuilding docs is automagic :-/
DEPENDENCIES="
    build:
        virtual/pkg-config[>=0.9.0]
    build+run:
        dev-libs/libxml2:2.0
        x11-libs/libpciaccess
        graphics? (
            x11-libs/cairo
            x11-libs/libX11  [[ note = [ X11/Xlib.h, X11/Xutil.h ] ]]
            x11-proto/xorgproto [[ note = [ X11/keysym.h ] ]]
        )
"

# needs:
#  --enable-cuda     cuda.h / libcudart
#  --enable-libnuma  numactl / libnuma
#  --enable-opencl   CL/cl_ext.h
#  --enable-gl       NVCtrl/NVCtrl.h
#  --enable-nvml     nvidia-ml
DEFAULT_SRC_CONFIGURE_PARAMS=(
    --enable-libxml2
    --disable-cuda
    --disable-gl
    --disable-libnuma
    --disable-nvml
    --disable-opencl
)

DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=( 'graphics cairo' )

