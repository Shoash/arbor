# Copyright 2007 Bryan Østergaard
# Distributed under the terms of the GNU General Public License v2

require gitlab

SUMMARY="Utilities for managing processes on your system"
DESCRIPTION="
A set of some small useful utilities that use the proc filesystem:

* fuser - Identifies processes using files or sockets
* killall - kills processes by name, e.g. killall -HUP named
* peekfd - shows the data travelling over a file descriptor
* prtstat - prints statistics of a process
* pslog - prints log path(s) of a process
* pstree - shows the currently running processes as a tree
"

DOWNLOADS="mirror://sourceforge/${PN}/${PNV}.tar.xz"

LICENCES="GPL-2"
SLOT="0"
PLATFORMS="~amd64 ~arm ~armv7 ~armv8 ~x86"
MYOPTIONS="
    ( parts: binaries data documentation )
    ( linguas: bg ca cs da de el eo es eu fi fr hr hu id it ja nb nl pl pt_BR
               pt ro ru sr sv uk vi zh_CN zh_TW )
"

DEPENDENCIES="
    build:
        sys-devel/gettext[>=0.19.3]
    test:
        dev-util/dejagnu
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --enable-ipv6
    --enable-nls
    --disable-apparmor
    --disable-harden-flags
    --disable-selinux
    # TODO: Replace those checks upstream with AC_CHECK_FUNCS([malloc realloc])
    ac_cv_func_malloc_0_nonnull=yes
    ac_cv_func_realloc_0_nonnull=yes
)

src_install() {
    default

    for fname in fuser killall pstree peekfd ; do
        if [[ ! -f ${IMAGE}/usr/$(exhost --target)/bin/${fname} ]]; then
            edo rm "${IMAGE}"/usr/share/man/man1/${fname}.1
        fi
    done

    expart binaries /usr/$(exhost --target)/bin
    expart data /usr/share
    expart documentation /usr/share/{doc,man}
}

