# Copyright 2016 Alex Elsayed <eternaleye@gmail.com>
# Distributed under the terms of the GNU General Public License v2

require py-pep517 [ backend=setuptools entrypoints=[ ${PN} ] ]
require zsh-completion

export_exlib_phases src_install

SUMMARY="A high performance build system"
DESCRIPTION="
Meson is a cross-platform build system designed to be both as fast
and as user friendly as possible. It supports many languages and
compilers, including GCC, Clang and Visual Studio. Its build
definitions are written in a simple non-Turing-complete DSL.
"
HOMEPAGE="https://mesonbuild.com/"

LICENCES="Apache-2.0"
SLOT="0"
MYOPTIONS="
    vim-syntax
"

# Requires cross/multiarch adjustments
RESTRICT="test"

DEPENDENCIES="
    run:
        sys-devel/ninja[>=1.8.2]
        virtual/pkg-config
    suggestion:
        dev-python/tqdm[python_abis:*(-)?] [[
            description = [ Show a progress bar when generating build.ninja ]
        ]]
"

meson-build_src_install() {
    py-pep517_src_install

    doman "${WORK}"/PYTHON_ABIS/$(python_get_abi)/"${PYTHON_WORK}"/man/*.1
    insinto /usr/share/polkit-1/actions
    doins "${WORK}"/PYTHON_ABIS/$(python_get_abi)/"${PYTHON_WORK}"/data/com.mesonbuild.install.policy
}

install_one_multibuild() {
    py-pep517_install_one_multibuild

    if option vim-syntax; then
        for dir in ftdetect ftplugin indent syntax ; do
            insinto /usr/share/vim/vimfiles/${dir}
            doins data/syntax-highlighting/vim/${dir}/${PN}.vim
        done
    fi

    # Remove man page and polkit policy in wrong location and install it into
    # the correct one later.
    edo rm -r "${IMAGE}"/usr/$(exhost --target)/share/{man,polkit-1}
    edo rmdir "${IMAGE}"/usr/$(exhost --target)/share

    dozshcompletion data/shell-completions/zsh/_meson
}

test_one_multibuild() {
    edo ${PYTHON} -B run_tests.py
}

