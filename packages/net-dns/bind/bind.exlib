# Copyright 2008 Ingmar Vanhassel <ingmar@exherbo.org>
# Copyright 2008-2016 Wulf C. Krueger <philantrop@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require systemd-service \
    autotools [ supported_autoconf=[ 2.7 ] supported_automake=[ 1.16 ] ] \
    option-renames [ renames=[ 'postgres postgresql' ] ]

export_exlib_phases src_prepare src_configure src_install pkg_postinst

MY_PV=${PV/_p/-P}
MY_PNV=${PN}-${MY_PV}

SUMMARY="Berkeley Internet Name Domain"
DESCRIPTION="
The Berkeley Internet Name Domain (BIND) implements an Internet name server for
Unix operating systems. BIND consists of a server (or \`daemon') called \`named'
and a resolver library.
A name server is a network service that enables clients to name resources or objects
and share this information with other objects in the network.
"
HOMEPAGE="https://www.isc.org/software/${PN}"
DOWNLOADS="
    https://downloads.isc.org/isc/${PN}${PV%%.*}/${MY_PV}/${MY_PNV}.tar.xz
    https://www.internic.net/domain/named.cache
"

UPSTREAM_DOCUMENTATION="https://www.isc.org/downloads/${PN}/doc"
UPSTREAM_RELEASE_NOTES="https://ftp.isc.org/isc/${PN}${PV%%.*}/${MY_PV}/RELEASE-NOTES-${PN}-${MY_PV}.html"

LICENCES="MPL-2.0"
SLOT="0"
MYOPTIONS="
    berkdb
    caps
    geoip [[ description = [ ACLs can also be used for geographic access restrictions. ] ]]
    idn
    kerberos
    ldap
    mysql
    postgresql [[ description = [ Adds support for the PostgreSQL backend. ] ]]
    kerberos? ( ( providers: heimdal krb5 ) [[ number-selected = exactly-one ]] )
    ( providers: libressl openssl ) [[ number-selected = exactly-one ]]
"

# Tons of sandbox violations.
RESTRICT="test"

DEPENDENCIES="
    build:
        virtual/pkg-config
    build+run:
        dev-libs/json-c:=[>=0.11]
        dev-libs/libuv[>=1.37.0]
        dev-libs/libxml2:2.0[>=2.6.0]
        sys-libs/zlib
        berkdb? ( sys-libs/db:= )
        caps? ( sys-libs/libcap[>=2.1.0] )
        geoip? ( net-libs/libmaxminddb )
        idn? ( net-dns/libidn2:= )
        kerberos? (
            providers:heimdal? ( app-crypt/heimdal )
            providers:krb5? ( app-crypt/krb5 )
        )
        ldap? ( net-directory/openldap )
        mysql? ( virtual/mysql )
        postgresql? ( dev-db/postgresql-client )
        providers:libressl? ( dev-libs/libressl:=[>=2.7.0] )
        providers:openssl? ( dev-libs/openssl:=[>=1.0.0] )
        group/named
        user/named
    run:
        net-dns/bind-tools[~${PV}][geoip=][idn=][kerberos=][providers:*=]
    suggestion:
        postgresql? ( dev-db/postgresql:* )
"

WORK=${WORKBASE}/${MY_PNV}

# Parallel make is not supported upstream
DEFAULT_SRC_COMPILE_PARAMS=( -j1 )
DEFAULT_SRC_INSTALL_EXTRA_DOCS=( KNOWN-DEFECTS )

bind_src_prepare() {
    default

    # TODO: Report upstream
    edo sed -i "/AC_PATH_PROG(AR/d" configure.ac
    edo sed -i 's/"nm"/"'$(exhost --tool-prefix)'nm"/' util/mksymtbl.pl

    eautoconf
}

bind_src_configure() {
    myoption() {
        if option ${2}; then
            echo "--${1}-${3:-$(optionfmt ${2} )}=yes"
        else
            echo "--${1}-${3:-$(optionfmt ${2} )}=no"
        fi
    }

    econf \
        --includedir=/usr/$(exhost --target)/include \
        --localstatedir=/var \
        --sysconfdir=/etc/${PN} \
        --enable-auto-validation \
        --enable-buffer-useinline \
        --enable-dnsrps \
        --enable-dnsrps-dl \
        --enable-largefile=yes \
        --disable-dnstap \
        --disable-fips-mode \
        --disable-static \
        --with-dlz-filesystem=yes \
        --with-dlz-odbc=no \
        --with-dlz-stub=yes \
        --with-json-c \
        --with-libtool=yes \
        --with-libxml2=yes \
        --with-openssl=/usr/$(exhost --target) \
        --with-pkcs11=no \
        --with-zlib \
        --without-cmocka \
        --without-lmdb \
        --without-python \
        $(option_enable caps linux-caps) \
        $(option_enable geoip) \
        $(myoption with berkdb dlz-bdb) \
        $(myoption with geoip maxminddb) \
        $(myoption with idn libidn2) \
        $(myoption with kerberos gssapi) \
        $(myoption with ldap dlz-ldap) \
        $(myoption with mysql dlz-mysql) \
        $(myoption with postgresql dlz-postgres)
}

# Note to self or anyone who's going to work on bind: It will fail with a cryptic
# error ("RUNTIME_CHECK(dst_initialized == isc_boolean_true) failed") if you try
# to disable pkcs11 with either of the following switches:
#        --with-pkcs11=no \
#        --without-pkcs11 \
# *Not* using any switches turns pkcs11 off, though... Upstreams...

bind_src_install() {
    default

    #remove conflicts with bind-tools
    local BIND_TOOL BIND_TOOLS=( delv.1 dig.1 host.1 nslookup.1 nsupdate.1 dnssec-keygen.8 )
    for BIND_TOOL in "${BIND_TOOLS[@]}"; do
        edo rm "${IMAGE}"/usr/$(exhost --target)/bin/${BIND_TOOL%.*}
        edo rm "${IMAGE}"/usr/share/man/man${BIND_TOOL#*.}/${BIND_TOOL}
    done

    keepdir /etc/${PN} /var/${PN}/{pri,sec}
    edo chown named:named "${IMAGE}"/var/${PN}{,/{pri,sec}}

    # Install a basic configuration.
    insinto /etc/${PN}
    doins "${FILES}"/named.conf

    # documentation and a sample with everything bind can do.
    dodoc "${WORK}"/bin/named/named.conf.rst
    newins "${WORK}"/bin/tests/named.conf named.conf.complex_sample

    # Install the Administrator Reference Manual (ARM)
    docinto Administrator_Reference_Manual
    dodoc doc/arm/*.rst

    docinto misc
    dodoc doc/misc/*

    # Creating a CONFIG_PROTECT file for /var/bind
    hereenvd 30bind <<EOF
CONFIG_PROTECT="/var/bind"
EOF

    insinto /usr/$(exhost --target)/lib/tmpfiles.d
    hereins ${PN}.conf <<EOF
d /run/named 0755 named named -
EOF

    install_systemd_files

    # Install standard zones and root cache
    insinto /var/bind
    doins "${FETCHEDDIR}"/named.cache
    insinto /var/bind/pri
    doins "${FILES}"/{127,localhost}.zone
}

bind_pkg_postinst() {
    if [[ ! -f /etc/bind/rndc.key ]]; then
        if [[ -c /dev/urandom ]]; then
            nonfatal edo /usr/${host}/bin/rndc-confgen -r /dev/urandom -a -u named
        else
            nonfatal edo /usr/${host}/bin/rndc-confgen -a -u named
        fi
    fi
}

